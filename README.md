Example Spring JMS Client for SIS Connect Feed
==============================================

**THIS CODE IS PROVIDED ON AN "AS-IS" BASIS FOR GENERAL GUIDANCE, WITH AN EXISTING UNDERSTANDING OF SPRING APIS AND CONVENTIONS BEING PRESUMED. SIS CANNOT PROVIDE GENERAL SUPPORT FOR USE OF THE SPRING FRAMEWORK, WHICH IS DOCUMENTED AND DISCUSSED EXTENSIVELY ONLINE. PLEASE SEE https://bitbucket.org/sisbetting/customer_sampleclient FOR ADDITIONAL SAMPLES USING OTHER LANGUAGES AND/OR FRAMEWORKS**

To build and run this project standalone use

    mvn package
    java  -jar target/SpringJMSClient-1.0.jar

To run this project during development use

    mvn spring-boot:run

To compile code changes and run the standalone tests during development use

    mvn test

To build as a WAR (e.g. to deploy to a Java EE application server), see http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#build-tool-plugins-maven-packaging


Prerequisites
=============

Open/Oracle/IBM Java 8 or later recommended. May work with Java 6/7 - compatible at the code level but not tested.

Oracle Java (only) requires the JCE Unlimited Strength Policy Files to be installed from http://bit.ly/1beztkB otherwise SSL negotiation with our servers will fail.

Maven - version 3.0 or later. Download from http://maven.apache.org


Configuration
=============

Set whether to run the "simple" profile example (using a single feed processor) or "split-feed" example
(which sends messages related to dog or horse racing to separate message processors), by configuring the
active profile in application.yml.

Queue connection parameters are also set via application.yml. See the Spring documentation for how to set
this up with different profiles for different environments: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html

Check with your Account Manager or SIS Technical Contact on how to obtain connection credentials and client certificates.

The JMS client connection is configured in AppConfig.connectionFactory(). If deploying to a Java EE
application server you will probably prefer to have it manage JMS connections and change this method to
lookup a JNDI resource instead.


Consuming the Feed
==================

The feed can be consumed by a single process, or split for multiple consumers using message selectors.

The key to this is the MessageReceiver class, which receives all messages and delegates processing to
processor objects. In the simplest case, to have all messages consumed by one processor, configure as
follows:

```java
public class MessageReceiver {
    @Autowired
    private IMessageProcessor genericMessageProcessor;
    @Autowired
    private ReceiveUtil receiveUtil;

    @JmsListener(containerFactory = "genericContainerFactory",
                 destination = "${sis-q-connection.queue}")
    public void receiveEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(genericMessageProcessor, message, headers);
    }

}
```
The above example requires the sample GenericMessageProcessor component to be available in the application context (as per the "simple" configuration profile described above), with code added to process messages.

If it would be helpful to split the feed, e.g. to separate receipt of messages for dog and horse racing, this can be achieved by message selectors. See the SIS Connect Wiki for available message properties that can be used to split feed processing: https://siswiki.sis.tv:8090/display/SC/High+Level+Overview

For example, to process horse racing, BAGS and non-BAGS dog racing independently, would be achieved like this:
*Please note, the subCode property is not available yet. It's there just to show how the feed can be split
and processed separately.*

```java
public class MessageReceiver {
    @Autowired
    private IMessageProcessor horseRaceMessageProcessor;
    @Autowired
    private IMessageProcessor bagsRaceMessageProcessor;
    @Autowired
    private IMessageProcessor nonBagsRaceMessageProcessor;
    @Autowired
    private ReceiveUtil receiveUtil;

    @JmsListener(containerFactory = "horseRaceContainerFactory",
                 destination = "${sis-q-connection.queue}",
                 selector = "categoryCode='HR'")
    public void receiveHorseEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(horseRaceMessageProcessor, message, headers);
    }

    @JmsListener(containerFactory = "bagsRaceContainerFactory",
                 destination = "${sis-q-connection.queue}",
                 selector = "categoryCode='DG' and subCode='BG'")
    public void receiveBagsEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(bagsRaceMessageProcessor, message, headers);
    }

    @JmsListener(containerFactory = "nonBagsRaceContainerFactory",
                 destination = "${sis-q-connection.queue}",
                 selector = "categoryCode='DG' and subCode='NB'")
    public void receiveNonBagsEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(nonBagsRaceMessageProcessor, message, headers);
    }
}
```
**Error Handling**

Basic error handling is included in the sample code, for:

- "Poison messages" - messages that are invalid and could therefore never be processed - which are diverted to a Dead Letter Log (endpoint 
configurable via logback.xml). While poison messages should never be encountered in production they need to be accommodated as a possibility, to prevent the queue becoming blocked
 
- Unhandled exceptions, resulting in transactional redelivery (assuming that there is some downstream problem with e.g. a web service or a database), logging an error - i.e. to be picked up by an external monitor - and delaying redelivery with exponential backoff