package tv.sis.connect.client;

import org.apache.qpid.jms.JmsConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.connection.JmsTransactionManager;
import org.springframework.util.ErrorHandler;
import org.springframework.util.backoff.ExponentialBackOff;

import javax.jms.ConnectionFactory;

/**
 * All configuration is managed in this class - runtime properties and Spring context
 */
@Configuration
@EnableJms
@ComponentScan
public class AppConfig {

    private static Logger LOG = LoggerFactory.getLogger(AppConfig.class);

    /**
     * Enable "${propname}" substitution in @JmsListener etc
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        PropertySourcesPlaceholderConfigurer result = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(new ClassPathResource("application.yml"));
        result.setProperties(yaml.getObject());
        return result;
    }

    @Bean
    public ConnectionFactory connectionFactory(@Value("${sis-q-connection.broker-uri}") String brokerUri,
                                               @Value("${sis-q-connection.username}") String username,
                                               @Value("${sis-q-connection.password}") String password)
        //NB this uses a Spring listener container and MUST use a spring connection factory - either SCF or CCF. Otherwise consuming will stop after 1 message!
        CachingConnectionFactory result = new CachingConnectionFactory(new JmsConnectionFactory(username, password, brokerUri));
        result.setClientId(username);
        return result;
    }

    @Bean
    @Profile("simple")
    public JmsListenerContainerFactory genericMessageContainerFactory(ConnectionFactory connectionFactory) {
        return createListenerContainerFactory(connectionFactory);
    }

    public static JmsListenerContainerFactory createListenerContainerFactory(ConnectionFactory connectionFactory) {
        // Note the default listener starts a single consumer by default, maintaining message ordering
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        // Uncomment next line if deploying to a Java EE server rather than running standalone
        //factory.setTransactionManager(new JtaTransactionManager());
        factory.setTransactionManager(new JmsTransactionManager(connectionFactory));
        // Use transactions (roll back for redelivery if a processor fails downstream)
        factory.setSessionTransacted(true);
        // Set single threaded per listener to maintain message order
        factory.setConcurrency("1");
        // Attempt reconnections with exponential backoff
        factory.setBackOff(new ExponentialBackOff(1000, 1.5));
        // Wait before trying again if downstream processing fails
        factory.setErrorHandler(new  ErrorHandler() {
            @Override
            public void handleError(Throwable t) {
                try {
                    LOG.debug("Failed to consume message [{}], sleeping 1s before retry", t.getMessage());
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        });
        return factory;
    }

}
