package tv.sis.connect.client.framework;

import java.util.Map;

/**
 * Interface for POJO message processors with no JMS dependencies, invoked by MessageReceiver
 */
public interface IMessageProcessor {
    /**
     * Method to be implemented by all message processor implementations, to be called by a MessageReceiver
     *
     * @param message The message body as a String
     * @param headers The message headers as a Map
     * @throws Exception to let Spring handle it e.g. a downstream SQL or JMS exception to trigger an auto-retry
     */
    void processMessage(String message, Map headers) throws Exception;

}
