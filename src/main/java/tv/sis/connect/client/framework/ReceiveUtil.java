package tv.sis.connect.client.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.Map;

/**
 * Utility methods for processing received messages
 */
@Component
public class ReceiveUtil {
    // Dead letter log for poison messages
    private static final Logger DLC = LoggerFactory.getLogger("SISConnectDeadLetter");

    public void processMessage(IMessageProcessor processor, Message inMessage, Map headers) throws Exception {
        String message;
        if (inMessage instanceof TextMessage) {
            message = ((TextMessage) inMessage).getText();
        } else {
            DLC.error("Message of wrong type sent to {}: {} [{}]", processor.getClass().getSimpleName(),
                    inMessage.getClass().getSimpleName(), inMessage);
            return;
        }
        processor.processMessage(message, headers);
    }

}
