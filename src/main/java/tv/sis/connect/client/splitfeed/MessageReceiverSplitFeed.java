package tv.sis.connect.client.splitfeed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import tv.sis.connect.client.framework.IMessageProcessor;
import tv.sis.connect.client.framework.ReceiveUtil;

import javax.jms.Message;
import java.util.Map;

/**
 * This example receives specific messages using selectors and routes them to the appropriate processor. The feed
 * can be split further in the same manner e.g. BAGS / non-BAGS
 */
@Component
@Profile("split-feed")
public class MessageReceiverSplitFeed {

    @Autowired
    private IMessageProcessor dogRaceMessageProcessor;
    @Autowired
    private IMessageProcessor horseRaceMessageProcessor;
    @Autowired
    private ReceiveUtil receiveUtil;

    @JmsListener(containerFactory = "dogRaceContainerFactory",
                 destination = "${sis-q-connection.queue-name}", selector = "categoryCode='DG'")
    public void receiveDogEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(dogRaceMessageProcessor, message, headers);
    }

    @JmsListener(containerFactory = "horseRaceContainerFactory",
            destination = "${sis-q-connection.queue-name}", selector = "categoryCode='HR'")
    public void receiveHorseEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(horseRaceMessageProcessor, message, headers);
    }

}