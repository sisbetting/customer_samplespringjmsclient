This package contains a more complex example, with parallel processing of dog and horse racing.

To activate it, set the profile to "split-feed" in application.yml
