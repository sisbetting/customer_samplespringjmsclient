package tv.sis.connect.client.splitfeed;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.config.JmsListenerContainerFactory;
import tv.sis.connect.client.AppConfig;

import javax.jms.ConnectionFactory;

@Configuration
public class SplitFeedAppConfig {

   @Bean
   @Profile("split-feed")
   public JmsListenerContainerFactory dogRaceContainerFactory(ConnectionFactory connectionFactory) {
      return AppConfig.createListenerContainerFactory(connectionFactory);
   }
   @Bean
   @Profile("split-feed")
   public JmsListenerContainerFactory horseRaceContainerFactory(ConnectionFactory connectionFactory) {
      return AppConfig.createListenerContainerFactory(connectionFactory);
   }

}
