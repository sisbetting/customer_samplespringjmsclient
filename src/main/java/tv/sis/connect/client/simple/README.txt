This package contains a simple example, with all processing in a single thread.

To activate it, set the profile to "simple" in application.yml
