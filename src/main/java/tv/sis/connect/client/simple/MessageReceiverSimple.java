package tv.sis.connect.client.simple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.stereotype.Component;
import tv.sis.connect.client.framework.IMessageProcessor;
import tv.sis.connect.client.framework.ReceiveUtil;

import javax.jms.Message;
import java.util.Map;

/**
 * This simple example receives all messages from MessageReceiverSimple
 */
@Component
@Profile("simple")
public class MessageReceiverSimple {

    @Autowired
    private IMessageProcessor messageProcessor;
    @Autowired
    private ReceiveUtil receiveUtil;

    @JmsListener(containerFactory = "genericMessageContainerFactory",
                 destination = "${sis-q-connection.queue-name}")
    public void receiveRaceEvent(Message message, @Headers Map headers) throws Exception {
        receiveUtil.processMessage(messageProcessor, message, headers);
    }

}