package tv.sis.connect.client.simple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import tv.sis.connect.client.framework.IMessageProcessor;

import java.util.Map;

/**
 * Example processor for any race -related message, invoked by MessageReceiverSimple
 */
@Component
@Profile("simple")
public class MessageProcessor implements IMessageProcessor {
    private Logger LOG = LoggerFactory.getLogger(this.getClass());
    private volatile int messageCount = 0;

    @Override
    public void processMessage(String message, Map headers) {
        LOG.debug("Received race message: {} with headers {}", message, headers);
        // Add processing here e.g. to convert to object form using jaxb and send to a local queue or database.
        // NB if a message cannot be processed for some reason to do with invalid headers or content, just log
        // it as a dead letter without throwing an exception.

        messageCount++;
    }

    public int getMessageCount() {
        return messageCount;
    }
}
