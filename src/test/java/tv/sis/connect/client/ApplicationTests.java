package tv.sis.connect.client;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import tv.sis.connect.client.splitfeed.DogRaceMessageProcessor;
import tv.sis.connect.client.splitfeed.HorseRaceMessageProcessor;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Local integration tests
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class, TestConfig.class}, loader=AnnotationConfigContextLoader.class)
@ActiveProfiles(profiles = "split-feed")
public class ApplicationTests {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Value("${sis-q-connection.queue-name}")
    private String queueName;

    @Autowired
    private JmsTemplate sendTemplate;

    @Autowired
    private TestDogProcessor dogRaceMessageProcessor;
    @Autowired
    private TestHorseProcessor horseRaceMessageProcessor;

    /**
     * DogRaceMessageProcessor for testing that can artificially throw an Exception
     */
    public static class TestDogProcessor extends DogRaceMessageProcessor {
        public boolean goBang;
        public int deliveryCount;
        public int deliveredCount;
        public Map headers;
        private String message;

        @Override
        public void processMessage(String message, Map headers) throws Exception {
            deliveryCount++;
            super.processMessage(message, headers);
            this.headers = headers;
            this.message = message;
            if (goBang) {
                goBang = false;
                throw new IOException("Bang!");
            }
            deliveredCount++;
        }
    }

    public static class TestHorseProcessor extends HorseRaceMessageProcessor {
        public int deliveryCount;
        @Override
        public void processMessage(String message, Map headers) throws Exception {
            deliveryCount++;
            super.processMessage(message, headers);
        }
    }

    @Before
    public void setup() {
        dogRaceMessageProcessor.goBang = false;
        dogRaceMessageProcessor.deliveryCount = 0;
        dogRaceMessageProcessor.deliveredCount = 0;
        dogRaceMessageProcessor.headers = null;
    }

    /**
     * Test for happy path to ensure that messages are being routed to the correct
     * IMessageProcessor instance by MessageReceiverSplitFeed, according to the message headers
     */
    @Test
    public void testSubscription() throws Exception {
        LOG.debug("Running subscription test");
        sendTemplate.convertAndSend(queueName, "Dog test message", new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws JMSException {
                message.setStringProperty("categoryCode", "DG");
                return message;
            }
        });
        sendTemplate.convertAndSend(queueName, "Horse test message", new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws JMSException {
                message.setStringProperty("categoryCode", "HR");
                return message;
            }
        });
        waitFor(1, 1);
        Assert.assertEquals(1, horseRaceMessageProcessor.getMessageCount());
        Assert.assertEquals(1, dogRaceMessageProcessor.deliveredCount);
    }

    /**
     * Test that poison message is removed to unblock queue
     */
    @Test
    public void testPoisonMessage() throws Exception {
        LOG.debug("Running poison message test");
        // bad one
        sendTemplate.convertAndSend(queueName, new HashMap(), new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws JMSException {
                message.setStringProperty("categoryCode", "DG");
                return message;
            }
        });
        // followed by good one - should not be blocked
        sendTemplate.convertAndSend(queueName, "Dog test message", new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws JMSException {
                message.setStringProperty("categoryCode", "DG");
                return message;
            }
        });
        waitFor(2, 0);
        // Should only receive the second message
        Assert.assertEquals(1, dogRaceMessageProcessor.deliveryCount);
        Assert.assertEquals(1, dogRaceMessageProcessor.deliveredCount);
        Assert.assertEquals(dogRaceMessageProcessor.message, "Dog test message");
    }

    /**
     * Test that an unhandled exception will result in a redelivery attempt
     */
    @Test
    public void testRedelivery() throws Exception {
        LOG.debug("Running redelivery test");
        dogRaceMessageProcessor.goBang = true;
        sendTemplate.convertAndSend(queueName, "Dog test message", new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws JMSException {
                message.setStringProperty("categoryCode", "DG");
                return message;
            }
        });
        waitFor(2, 0);
        Assert.assertEquals(2, dogRaceMessageProcessor.deliveryCount);
        Assert.assertEquals(1, dogRaceMessageProcessor.deliveredCount);
        Assert.assertTrue((Boolean) dogRaceMessageProcessor.headers.get("jms_redelivered"));
    }

    protected void waitFor(int howManyDogs, int howManyHorses) throws InterruptedException {
        int its=0;
        while (dogRaceMessageProcessor.deliveryCount < howManyDogs && its++ < 10)
            Thread.sleep(500);
        its=0;
        while (horseRaceMessageProcessor.deliveryCount < howManyHorses && its++ < 10)
            Thread.sleep(500);
    }

}
