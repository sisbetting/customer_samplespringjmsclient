package tv.sis.connect.client;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

/**
 * Test configuration using embedded Active MQ instead of remote IBM MQ
 */
@Configuration
public class TestConfig extends AppConfig {

    @Bean
    @Override
    public ConnectionFactory connectionFactory(String brokerUri, String username, String password) {
        CachingConnectionFactory result = new CachingConnectionFactory();
        result.setTargetConnectionFactory(new ActiveMQConnectionFactory(
                "vm://localhost?broker.persistent=false&broker.useJmx=false&broker.useShutdownHook=false"));
        return result;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        PropertySourcesPlaceholderConfigurer result = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(new ClassPathResource("application.yml"), new ClassPathResource("application-test.yml"));
        result.setProperties(yaml.getObject());
        return result;
    }

    /**
     * For sending test messages
     */
    @Bean
    public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
        JmsTemplate result = new JmsTemplate();
        result.setConnectionFactory(connectionFactory);
        return result;
    }

    /**
     * Override dog processor with test version
     */
    @Bean
    public ApplicationTests.TestDogProcessor dogRaceMessageProcessor() {
        return new ApplicationTests.TestDogProcessor();
    }

    @Bean
    public ApplicationTests.TestHorseProcessor horseRaceMessageProcessor() {
        return new ApplicationTests.TestHorseProcessor();
    }

}

